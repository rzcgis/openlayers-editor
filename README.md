# Openlayers 编辑器

本仓库基于Github[openlayers-editor](https://github.com/geops/openlayers-editor)，翻译并进行扩展。

![npm](/libs/ole.svg)
![Build](/libs/badge.svg)
![e2e tests](/libs/Cypress_badge.svg)
![code style](/libs/code_style-prettier-ff69b4.svg)
![Conventional Commits](/libs/ConventionalCommits-1.0.0-yellow.svg)

OpenLayers 编辑器 (OLE) 基于 [OpenLayers](https://openlayers.org/) 并 提供一系列扩展控件用来编辑空间数据.

欢迎捐款！请随意添加更多控件并扩展当前功能。
此外，构建过程目前非常基本，可以进行优化。
也可以进行翻译。

## 主要功能

- CAD tool（吸附工具）实现几何对象的对齐吸附
- 可以绘制点线面要素
- 移动和旋转几何对象
- 编辑几何对象
- 删除几何对象
- 拓扑操作使用[JSTS](https://github.com/bjornharrtell/jsts): 缓冲, 联合, 合并, 求差
- 用于激活和停用控件的工具栏
- 各类军标

![主界面](./img/4README/main.png)

## 示例

示例请访问[https://openlayers-editor.geops.com](https://openlayers-editor.geops.com).

## 依赖

- node & npm

## 开始

- Clone 仓库 `git clone https://gitee.com/rzcgis/openlayers-editor.git`
- Install: `npm install`
- Build: `npm run build`
- Run: `npm start`
- 浏览器访问 [http://localhost:8080](http://localhost:8080)

## 使用

```html
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/ole@latest/style/ole.css" />
<script src="https://cdn.jsdelivr.net/npm/ole@2.1.2/build/index.js"></script>
```

```js
var editor = new ole.Editor(map);

var cad = new ole.control.CAD({
  source: editLayer.getSource()
});

var draw = new ole.control.Draw({
  source: editLayer.getSource()
});

editor.addControls([draw, cad]);

```
> 支持的标绘类型

| key                                    | type | desc      |
|:---------------------------------------| :--- |:----------|
| `plot:ARC`                 | `string` | 弓形        |
| `plot:ELLIPSE`             | `string` | 椭圆        |
| `plot:CURVE`               | `string` | 曲线        |
| `plot:CLOSED_CURVE`        | `string` | 闭合曲面      |
| `plot:LUNE`                | `string` | 弓形        |
| `plot:SECTOR`              | `string` | 扇形        |
| `plot:GATHERING_PLACE`     | `string` | 集结地       |
| `plot:STRAIGHT_ARROW`      | `string` | 细直箭头      |
| `plot:ASSAULT_DIRECTION`   | `string` | 粗单直箭头     |
| `plot:ATTACK_ARROW`        | `string` | 进攻方向      |
| `plot:TAILED_ATTACK_ARROW` | `string` | 进攻方向（尾）   |
| `plot:SQUAD_COMBAT`        | `string` | 战斗行动      |
| `plot:TAILED_SQUAD_COMBAT` | `string` | 分队战斗行动（尾） |
| `plot:FINE_ARROW`          | `string` | 粗单尖头箭头    |
| `plot:CIRCLE`              | `string` | 圆         |
| `plot:DOUBLE_ARROW`        | `string` | 双箭头       |
| `plot:POLYLINE`            | `string` | 线         |
| `plot:FREEHAND_LINE`       | `string` | 自由线       |
| `plot:POLYGON`             | `string` | 面         |
| `plot:FREEHAND_POLYGON`    | `string` | 自由面       |
| `plot:RECTANGLE`           | `string` | 矩形        |
| `plot:RectInclined1`       | `string` | 斜矩形1      |
| `plot:RectInclined2`       | `string` | 斜矩形2      |
| `plot:MARKER`              | `string` | 点         |
| `plot:TEXTAREA`            | `string` | 文本框       |

## 开发

- Install: `npm install`
- Build: `npm run build`
- Create doc: `npm run-script doc`

### 版本和变更日志

这个仓库使用 [standard-version](https://github.com/conventional-changelog/standard-version/) 发布版本控制和变更日志管理. 因此，应使用提交更新 [conventional commit](https://www.conventionalcommits.org/en/v1.0.0/) :

```text

<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

提交包含以下结构元素，用于向库的使用者正确传达意思:

1. fix: 类型修复的提交会修补代码库中的错误（这与语义版本控制中的PATCH相关）.
2. feat: feat类型的提交为代码库引入了一个新特性（这与语义版本控制中的MINOR相关）.
3. BREAKING CHANGE: 具有页脚BREAKING CHANGE:的提交，或附加！在类型/范围之后，引入了一个破坏性的API更改（与语义版本控制中的MAJOR相关）。BREAKING CHANGE可以是任何类型的提交的一部分.
4. 允许fix:和feat:以外的类型，例如@commitlint/config-contropical（基于Angular约定）建议build:、chore:、ci:、docs:、style:、refract:、perf:、test:和其他类型。
5.除BREAKING CHANGE之外的页脚：可以提供＜description＞，并遵循类似于git尾部格式的约定。

常规委员会规范没有强制要求附加类型，并且在语义版本控制中没有隐含影响（除非它们包括BREAKING CHANGE）。范围可以提供给提交的类型，以提供额外的上下文信息，并包含在括号中，例如，feat（解析器）：添加解析数组的能力。
