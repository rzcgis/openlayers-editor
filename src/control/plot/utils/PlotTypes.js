/**
 * Created by FDD on 2017/5/20.
 */
const TEXTAREA = 'TextArea'; // 文本标绘（特殊）
const ARC = 'Arc';
const CURVE = 'Curve';
const GATHERING_PLACE = 'GatheringPlace';
const POLYLINE = 'Polyline';
const FREEHANDLINE = 'FreeHandLine';
const POINT = 'Point';
const PENNANT = 'Pennant';
const RECTANGLE = 'RectAngle';
const CIRCLE = 'Circle';
const ELLIPSE = 'Ellipse';
const LUNE = 'Lune';
const SECTOR = 'Sector';
const CLOSED_CURVE = 'ClosedCurve';
const POLYGON = 'Polygon';
const FREE_POLYGON = 'FreePolygon';
const ATTACK_ARROW = 'AttackArrow';
const DOUBLE_ARROW = 'DoubleArrow';
const STRAIGHT_ARROW = 'StraightArrow';
const FINE_ARROW = 'FineArrow';
const ASSAULT_DIRECTION = 'AssaultDirection';
const TAILED_ATTACK_ARROW = 'TailedAttackArrow';
const SQUAD_COMBAT = 'SquadCombat';
const TAILED_SQUAD_COMBAT = 'TailedSquadCombat';
const RECTFLAG = 'RectFlag';
const TRIANGLEFLAG = 'TriangleFlag';
const CURVEFLAG = 'CurveFlag';
const RECTINCLINED1 = 'RectInclined1';
const RECTINCLINED2 = 'RectInclined2';

const Alias_TEXTAREA = '文本'; // 文本标绘（特殊）
const Alias_ARC = '弓形';
const Alias_CURVE = '曲线';
const Alias_GATHERING_PLACE = '集结地';
const Alias_POLYLINE = '折线';
const Alias_FREEHANDLINE = '自由线';
const Alias_POINT = '目标';
const Alias_PENNANT = '三角旗';
const Alias_RECTANGLE = '矩形';
const Alias_CIRCLE = '圆';
const Alias_ELLIPSE = '椭圆';
const Alias_LUNE = '弓形';
const Alias_SECTOR = '扇形';
const Alias_CLOSED_CURVE = '闭合曲面';
const Alias_POLYGON = '多边形';
const Alias_FREE_POLYGON = '自由多边形';
const Alias_ATTACK_ARROW = '进攻方向';
const Alias_DOUBLE_ARROW = '双箭头';
const Alias_STRAIGHT_ARROW = '细直箭头';
const Alias_FINE_ARROW = '斜箭头';
const Alias_ASSAULT_DIRECTION = '粗单直箭头';
const Alias_TAILED_ATTACK_ARROW = '燕尾曲箭头';
const Alias_SQUAD_COMBAT = '曲箭头';
const Alias_TAILED_SQUAD_COMBAT = '分队战斗行动（尾）';
const Alias_RECTFLAG = '矩形旗';
const Alias_TRIANGLEFLAG = '三角旗';
const Alias_CURVEFLAG = '曲线标志旗';
const Alias_RECTINCLINED1 = '斜矩形1';
const Alias_RECTINCLINED2 = '斜矩形2';

export {
  TEXTAREA,
  ARC,
  CURVE,
  GATHERING_PLACE,
  POLYLINE,
  FREEHANDLINE,
  POINT,
  PENNANT,
  RECTANGLE,
  CIRCLE,
  ELLIPSE,
  LUNE,
  SECTOR,
  CLOSED_CURVE,
  POLYGON,
  FREE_POLYGON,
  ATTACK_ARROW,
  DOUBLE_ARROW,
  STRAIGHT_ARROW,
  FINE_ARROW,
  ASSAULT_DIRECTION,
  TAILED_SQUAD_COMBAT,
  TAILED_ATTACK_ARROW,
  SQUAD_COMBAT,
  RECTFLAG,
  TRIANGLEFLAG,
  CURVEFLAG,
  RECTINCLINED1,
  RECTINCLINED2,
  Alias_TEXTAREA,
  Alias_ARC,
  Alias_CURVE,
  Alias_GATHERING_PLACE,
  Alias_POLYLINE,
  Alias_FREEHANDLINE,
  Alias_POINT,
  Alias_PENNANT,
  Alias_RECTANGLE,
  Alias_CIRCLE,
  Alias_ELLIPSE,
  Alias_LUNE,
  Alias_SECTOR,
  Alias_CLOSED_CURVE,
  Alias_POLYGON,
  Alias_FREE_POLYGON,
  Alias_ATTACK_ARROW,
  Alias_DOUBLE_ARROW,
  Alias_STRAIGHT_ARROW,
  Alias_FINE_ARROW,
  Alias_ASSAULT_DIRECTION,
  Alias_TAILED_ATTACK_ARROW,
  Alias_SQUAD_COMBAT,
  Alias_TAILED_SQUAD_COMBAT,
  Alias_RECTFLAG,
  Alias_TRIANGLEFLAG,
  Alias_CURVEFLAG,
  Alias_RECTINCLINED1,
  Alias_RECTINCLINED2,
};
